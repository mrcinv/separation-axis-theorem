require 'separate'
# Rspec tests for separate.rb

RSpec.describe SeparatingAxisTheorem2D do
  describe '#normal' do
    it 'returns perpendicular vector' do
      expect(SeparatingAxisTheorem2D.normal([[0, 0], [1, 0]])).to eq([0, 1])
      expect(SeparatingAxisTheorem2D.normal([[1, 1], [3, 2]])).to eq([-1, 2])
    end
  end
  describe '#overlap?' do
    it 'returns false if one sets is greater than another' do
      expect(SeparatingAxisTheorem2D.overlap?([1, 2, 3], [4, 5, 6])).to be false
      expect(
        SeparatingAxisTheorem2D.overlap?([1, 2, 3], [-4, -5, -6])
      ).to be false
    end
    it 'returns true if max of the first is greater than min of the second' do
      expect(
        SeparatingAxisTheorem2D.overlap?([1, 2, 3], [2.5, 4, 5])
      ).to be true
      expect(SeparatingAxisTheorem2D.overlap?([3, 5, 6], [1, 2, 4])).to be true
    end
  end
  describe '#separating_axis_exists?' do
    square1 = [[0, 0], [0, 1], [1, 1], [1, 0]]
    square2 = [[2, 0], [3, 0], [3, 1], [2, 1]]
    square3 = [[0.5, 0.5], [0.5, 2], [2, 2], [2, 0.5]]
    it 'returns true for parallel squares' do
      expect(
        SeparatingAxisTheorem2D.separating_axis_exists?(square1, square2)
      ).to be true
    end
    it 'returns false for if one vertix is inside other polygon' do
      expect(
        SeparatingAxisTheorem2D.separating_axis_exists?(square1, square3)
      ).to be false
    end
    triangle1 = [[0, 1.5], [1.5, 1.5], [1.5, 0]]
    triangle2 = [[0, 1], [1, 0], [-1, -1]]
    it 'returns false if edges intersect' do
      expect(
        SeparatingAxisTheorem2D.separating_axis_exists?(square1, triangle1)
      ).to be false
    end
    it 'returns true for unintersecting triangles' do
      expect(
        SeparatingAxisTheorem2D.separating_axis_exists?(triangle1, triangle2)
      ).to be true
    end
    triangle3 = [[0.2, 0.2], [0.2, 0.6], [0.6, 0.2]]
    it 'returns false if one polygon lies inside another' do
      expect(
        SeparatingAxisTheorem2D.separating_axis_exists?(square1, triangle3)
      ).to be false
    end
  end
end
