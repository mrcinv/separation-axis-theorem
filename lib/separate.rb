# Collision detection in 2D
module SeparatingAxisTheorem2D
  # Calculate the normal to a segment between two points.
  #
  # @param edge [Array] a pair of two points consisting of an array of
  # x, y coordinates
  # @return [Array] of components of the normal to the segment given
  # by the counter clockwise rotation
  def self.normal(edge)
    # line direction = point2 - point1, rotation (x, y) -> (-y, x)
    point1 = edge[0]
    point2 = edge[1]
    [point1[1] - point2[1], point2[0] - point1[0]]
  end

  # Calculate the dot product of two vectors.
  #
  # @param vec1 [Array] array of components for the first vector
  # @param vec2 [Array] array of components for the second vector
  # @return [Number] a dot product of v1 and v2
  def self.dot(vec1, vec2)
    vec1.zip(vec2).map { |x| x[0] * x[1] }.sum
  end

  # Project the points to a line given by its direction vector.
  #
  # @param points [Array] an array of points.
  # Each point is an [Array] of its coordinates
  # @param direction [Array] directional vector of the line
  # @return [Array] of local coordinates on the line (numbers)
  # of the projected points.
  def self.project(points, direction)
    points.map { |point| dot(point, direction) }
  end

  # Find if the intervals, containing two sets of numbers overlap. Fund out if
  # all the numbers from the first set are smaller than all the numbers from
  # the second set, or if all the numbers from the first set are grater than all
  # the numbers from the second set.
  #
  # @param array1 [Array] first set of numbers
  # @param array2 [Array] second set of numbers
  # @return [Bool] True if projections do overlap, False otherwise
  def self.overlap?(array1, array2)
    min1 = array1.min
    min2 = array2.min
    max1 = array1.max
    max2 = array2.max
    !(min1 > max2 || max1 < min2)
  end

  # A list of edges of a polygon
  #
  # @param poly [Array] list of points on a polygon
  # @return [Array] a list of edges (pairs of points)
  def self.edges(poly)
    poly.zip(poly[1..-1].append(poly[0]))
  end

  # Find if two polygon intersect by
  # [separating axis theorem](https://en.wikipedia.org/wiki/Hyperplane_separation_theorem)
  # Two polygons do not intersect, if there exist a separating axis.
  # A separating axis is an axis(one dimensional subspace), such that
  # the projections of the two polygons to this axis do not intersect.
  # For convex polygon the converse is also true, that if the polygons do not
  # intersec, then there exist a separating axis. For convex polygons,
  # this method gives definitive answer, while for nonconvex polygon only
  # the negative answer is final.
  #
  # @param poly1 [Array] a sequence of points for the first polygon
  # @param poly2 [Array] a sequence of points for the second polygon
  # @return [Bool] returns True if separating axis exists,
  # False otherwise
  def self.separating_axis_exists?(poly1, poly2)
    [poly1, poly2].each do |poly|
      edges(poly).each do |edge|
        axis = normal(edge)
        return true unless overlap?(project(poly1, axis), project(poly2, axis))
      end
    end
    false
  end
end
